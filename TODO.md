# TODO

- Main APIs
  - No Access Token Required
    - [X] 5.1 Get Games Categories
    - [X] 5.2 Search Categories
    - [X] 5.3 Get top channels
    - [X] 5.4 Get users (get channel id)
    - [X] 5.5 Get channel info by ID
    - [X] 5.10 Get emotes
    - [X] 5.11 Get channel viewers
    - [X] 5.12 Get channel followers 
    - [X] 5.13 Get livestream urls
    - [X] 5.14 Get clips info
    - [X] 5.15 Get past streams info 
  - Access Token Required
    - [ ] 5.6 Read channel info with Stream Key 
    - [ ] 5.7 Edit channel information
    - [ ] 5.8 Get user info 
    - [ ] 5.9 Get subscribers 