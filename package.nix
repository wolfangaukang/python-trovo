{ lib
, poetry2nix
}:

poetry2nix.mkPoetryApplication {
  projectDir = lib.cleanSource ./.;
  meta = {
    description = "A Python wrapper for the Trovo API";
    homepage = "https://codeberg.org/wolfangaukang/python-trovo";
    license = lib.licenses.gpl3Only;
    maintainers = with lib.maintainers; [ wolfangaukang ];
  };
}
