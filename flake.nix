{
  description = "A Python wrapper for the Trovo API";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, poetry2nix, ... }:
    let
      systems = nixpkgs.lib.systems.flakeExposed;
      forEachSystem = nixpkgs.lib.genAttrs systems;
      pkgsFor = nixpkgs.lib.genAttrs systems (system: import nixpkgs {
        overlays = [ poetry2nix.overlays.default ];
        inherit system;
      });

    in
    {
      packages = forEachSystem (system: {
        python-trovo = pkgsFor.${system}.callPackage ./package.nix { };
        default = self.outputs.packages.${system}.python-trovo;
      });
      devShells = forEachSystem (system:
        let
          pkgs = pkgsFor.${system};
          inherit (pkgs) mkShell poetry;
          inherit (pkgs.python3Packages) python-lsp-server;
          devEnv = pkgs.poetry2nix.mkPoetryEnv { projectDir = pkgsFor.${system}.lib.cleanSource ./.; };

        in
        {
          default = mkShell { buildInputs = [ devEnv poetry python-lsp-server ]; };
        });
      overlays.default = final: prev: { inherit (self.packages.${final.system}) python-trovo; };
      formatter = forEachSystem (system: pkgsFor.${system}.nixpkgs-fmt);
    };
}
